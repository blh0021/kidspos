FROM node:14-alpine

COPY backend /home/node/backend
COPY frontend /home/node/frontend

WORKDIR /home/node/frontend
RUN yarn install && yarn build

WORKDIR /home/node/backend
RUN yarn install

ENTRYPOINT ["sh", "start.sh"]