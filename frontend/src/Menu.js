import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';

function Menu(props) {
  return (
    <Navbar bg="dark" variant="dark" className="mb-4">
      <Container>
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="/money-mouth.webp"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          Kids POS
        </Navbar.Brand>
        <Nav.Link onClick={props.onClick}>Pay</Nav.Link>
      </Container>
    </Navbar>
  );
}

export default Menu;