import "bootstrap/dist/css/bootstrap.min.css";

import React, { useRef, useState, useEffect } from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";

import Menu from "./Menu";

function App() {
  const [barcode, setBarcode] = useState();
  const [item, setItem] = useState(null);
  const [cart, setCart] = useState([]);
  const barcodeEl = useRef(null);

  useEffect(() => {
    barcodeEl.current.focus();
  }, [barcode]);

  const handleSubmit = (evt) => {
    evt.preventDefault();
    if (barcode != "") {
      fetch("/search?q=" + barcode)
        .then((response) => response.json())
        .then((data) => {
          let c = cart || [];
          c.push(data);
          setCart(c);
          setItem(data);
          setBarcode("");
          barcodeEl.current.focus();
        });
    }
  };

  const pay = () => {
    setCart([])
  }
  
  return (
    <div className="App">
      <Menu onClick={pay} />
      <Container>
        <Form onSubmit={handleSubmit}>
          <Form.Group as={Row} className="mb-3" controlId="formBasicEmail">
            <Form.Label column sm={2}>
              Scan
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="text"
                placeholder="barcode"
                value={barcode}
                ref={barcodeEl}
                onChange={(e) => setBarcode(e.target.value)}
              />
            </Col>
          </Form.Group>
        </Form>
      </Container>

      {item && (
        <Container>
          {cart.map((i) => (
            <Row>
              <Col><img src={"https://picsum.photos/200?" + i.barcode} /></Col>
              <Col><p>{i.barcode}</p></Col>
              <Col>$9.99</Col>
            </Row>
          ))}
        </Container>
      )}
    </div>
  );
}

export default App;
