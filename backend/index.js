const express = require("express");
const app = express();
const port = process.env.PORT || 4001;

app.use(express.static('../frontend/build/'))

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/search", (req, res) => {
    //console.log(req.query)
  if (req.query.q != "null") {
    res.json({
      barcode: req.query.q,
      img: "https://cdn.vox-cdn.com/thumbor/EjDNfBO5bmKTTKdyt5NuH_USUus=/1400x1400/filters:format(png)/cdn.vox-cdn.com/uploads/chorus_asset/file/22051473/Untitled.png",
      name: "test",
      cost: 9.99,
    });
  } else {
    res.json({
      barcode: "",
      img: "",
      name: "",
      cost: 0,
    });
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
